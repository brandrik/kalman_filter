**CONTENTS**

<!-- TOC -->

- [1. Bayesian Filters](#1-bayesian-filters)
  - [1.1. Basics](#11-basics)
  - [1.2. Bayes Filter](#12-bayes-filter)
  - [1.3. Gaussian Filter](#13-gaussian-filter)
    - [1.3.1. Kalman Filter](#131-kalman-filter)
    - [1.3.2. Extended Kalman Filter](#132-extended-kalman-filter)
    - [1.3.3. ES-EKF](#133-es-ekf)
    - [1.3.4. Unscented Kalman Filter](#134-unscented-kalman-filter)
  - [1.4. Kalman Filter bias estimation](#14-kalman-filter-bias-estimation)
    - [1.4.1. Information Filter](#141-information-filter)
    - [1.4.2. Histogram Filter](#142-histogram-filter)
    - [1.4.3. Particle filter](#143-particle-filter)
- [2. Sensor Fusion using Kalman Filter](#2-sensor-fusion-using-kalman-filter)
- [3. Appendix](#3-appendix)
- [4. Cholesky Decomposition (for Covariance matrix $\Sigma$)](#4-cholesky-decomposition-for-covariance-matrix-sigma)
      - [4.0.0.1. Positive definit matrix](#4001-positive-definit-matrix)
- [5. RESOURCES:](#5-resources)

<!-- /TOC -->

# 1. Bayesian Filters
<a id="markdown-bayesian-filters" name="bayesian-filters"></a>


## 1.1. Basics
<a id="markdown-basics" name="basics"></a>


**Update (Prediction):**

- consider new input $u_t$

**Measurement Update:**

- consider new measurement $z_t$

**Completeness of a state:**

A state $x_t$ is called complete, if it is the best predictor of the future. That is, knowledge of the past states, measurement and control carry **no** additional information that would help to predict the future more accurately. That is the current state only depends on the previous state and the current control input. 0

Markov Chain:
Is a temporal process meet the conditions of completeness of a state.

In practice it is impossible to specify a complete state ofr any realistic robot system.

Example:

X: State
U: input
Z: Measurement

```math
p(x_t|x_{0:t−1}, z_{1:t−1} , u_{1:t}) = p(x_t |  x_(t−1),u_t)
```

The above equation states, that certain variables are independent of others if one knows the values of a third group of variables, the conditioning variables.

FROM [1], Sec. 2.3.3:

**State Transition Probability**

$p(x_t|x_(t−1),u_t)$ : state transition probabilty

The state transition probability specifies the change of state over time according to controls u. (input to the system)

**Measurement Probability**

$p(z_t|x_t)$: measurement probability

The measurement probability specifies the probabilistic law according to which measurements $z$ are generated from the environment state $x$. It can be interpreted as: Measurements are noisy projections of the state.

From [1], Sec: 2.3.3:

The state transition probability and the measurement probability together describe the dynamical stochastic system of states of the robot and its environment. The state at time $t$ is stochastically dependent on the state at time $t-1$ and the control $u_t$. The measurement $z_t$ depends stochastically on the state at time t.

**Such a temporal generative model is also known as *Hidden Markov Model (HMM)* or *dynamic Bayes Network* (DBN).**

stochastical: dependent of the conincidence.

Belief Distributions:

A belief reflects the robots internal knowledge about the state of the environment. Some quantities are infered from sensor data measuring related ones - inference. Therefore the true state is distinguished from the robots internal belief with regard to the state.

Belief distributions are posterior probabilities over state variables conditioned on the variable data. Below is the posterior probability distribution over the state $x_t$  at time $t$ conditioned on all past measurements $z_{1:t}$ and all past controls $u_{1:t}$.

$bel(x_t) = p(x_t | z_{1:t}, u_{1:t})$

Prediction:

$\overline{bel}(x_t)=p(x_t∣z_{1:t-1}, u_{1:t})$

belief synonyms: state of knowledge, information state.

Measurement Update (Correction):

Calculating $\overline{bel}(x_t)$ from $bel(x_t)$

is called measurement update.

## 1.2. Bayes Filter
<a id="markdown-bayes-filter" name="bayes-filter"></a>


The bayes filter calculates the the posterior over state $x_t$

conditioned on the measurement and control data up to time $t$, provided the state is complete (Hidden Markov process).

Bayes filter is for continous space, in practice an approximation is used, e.g. Kalman.

Requirements for Bayes Filter:

1. initial belief: $p(x_0)$
2. measurement probability: $p(z_t | x_t)$
3. transition probability: $p(x_t | u_t, x_{t-1})$

Markov Assumption is may be violated by:

1. unmodeled dynamics not included in the state $x_t$, e.g. moving people and their effects on sensor measurements in the localizaion example
2. inaccuracies in the probabilistic models $p(z_t|x_t)$ and $p(x_t|u_t, x_{t-1})$, e.g. an error in the map for localizing robot.
3. approximation errors when using approximate representations of belieg functions (e.g. Gaussians ..)
4. software variables in the robot control software that influenence multiple controls (e.g. the variable "target location", typically influences an entire sequence of control commands)

Many of the above could be included in the state representations, but would be computatonally expensive. In practice, bayes filters have been found surprisingly robust against thos violations.

## 1.3. Gaussian Filter
<a id="markdown-gaussian-filter" name="gaussian-filter"></a>


- most tractable implementations of bayes filter
- belief is represented by multivariate normal distribution
- applications: the posterior is focused around the true state with a small margin of uncertainty

**Moments for moments parameterization**

1. $\sum$: Covariance of $x$, symmetric and pos. semidefinite matrix
2. $\mu$: mean value of $x$

Probability density function over variable $x$:

eq: 3.2

$$
p(x) = det(2 \pi \sum)^{-0.5} * e^{-0.5(x-µ)^T\sum^{-1}(x-µ)}

$$

example:

using unimodal ditributions (e.g. Gaussian: one peak in distribution -> unimodal)

example: Kalman Filter

### 1.3.1. Kalman Filter
<a id="markdown-kalman-filter" name="kalman-filter"></a>

- is a form of recursive least-square estimation, allows to combine information from a motion model and sensor measurements.
- implements BF using moments parameterization
- implements belief computation for continous states
- NOT applicable to discrete hybrid state spaces.
- assumes linear system dynamics.
- The KF uses the system model to make *predictions* of the state, and uses the measurements to make *correction* to the predictions.
- The Kalman Filter is the Best Linear Unbiased Estimator (BLUE) [[7]](#7)


![Kalman Filter](images/Kalman_Filter_pdfs.jpg)
  *Kalman Filter algorithm steps*

Posteriors are Gaussian if the following three properties hold, in addition to the Markov assumptions of the Bayes Filter.

1. The transition probability must be a linear function in its arguments with added Gaussian noise: eq. 3.2  $x_t = A_t x_{t-1} + B_t u_t + \epsilon_t$, $\epsilon_t$ models uncertainty introduced by the state transition: $µ_{\epsilon} = 0$,$Cov(\epsilon) = R_t$

Eq 3.2 defines the state transition probability $p(x_t | u_t, x_t{t-1})$, plugging 3.2 into the definition of the multivariate normal distribution yields to the posterior:

$$
p(x_t| u_t, x_{t-1}) = .. page 41.

$$

2. The measurement probability $p(z_t | x_t)$ must also be linear in its arguments, with added Gaussian noise:

$$
z_t = C_tx_t + \delta_t

$$

$\delta_t$: measurement noise

The distribution of $\delta_t$ is a multivariate Gaussian with zero mean and covariance $Q_t$.

3. The initial belief $bel(x_0)$ must be normally distributed (Gaussian), as well.

These three assumptions above are sufficient to ensure that the posterior $bel(x_t)$ is always Gaussian, for any point in time $t$.

**Updating the mean $µ$**

The mean is updated by substituting $x_{t-1}$ with $u_{t-1}$

**Updating Covariance:**

The update of the covariance considers the fact, that states depend on previous states through the linear matrix $A_t$. This matris is multiplied twice into the covariance, since the convariance is a quadratic matrix.

The prior belief is transformed to the posterior by incorporating the current measurement $z_t$.

**Kalman Gain**

- specifies the degree to which the measurement is incorporated into the new state estimate.

**Innovation:** (difference between actual actual measurement and expected one)

The mean $µ_t$ is manipulated by adjusting it in proportion to the Kalman Gain $K_t$ and the deviation of the measurement, $z_t$, and the expected measurement $C_t µ_t$

$$
µ_t  = \bar{µ} + K_t(z_t-C_t\bar{µ}_t)

$$

**Adjusting information gain:**

In the final step of the Kalman Filter algorithm, the new covariance of the posterior belief is calculated, adjusting for the information gain resulting from the measurement.

$$
\sum_t = (I - K_tC_t)\bar{\sum}_t

$$

**Computational Complexity**

- best algorithm for matrix inversion $O(d^{2.4})$ for $d$x$d$ matrix
- $O$: Landau Symbol, defines upper bound
- Kalman Filter's computational complexity (lower bound): $O(k^{2.4})$, $k$: dimension of measurement vector
- The KF's computational complexity is mainly dominated by $O(n^2)$, where $n$ is the dimension of the state space.

The Kalman filter alternates between measurement update step and prediction step:

1. measurement update step: sensor data is integrated into the present belief --> decreasing uncertainty
2. prediction step (=control update step): increasing uncertainty in the robot's belief.

### 1.3.2. Extended Kalman Filter
<a id="markdown-extended-kalman-filter" name="extended-kalman-filter"></a>

- linearizes the process function around the mean of the previous time step
- extends KF to slightly non-linear problems
- EKF: belief is only approximate, not exact as in the case of KF
- EKF goal has shifted from computing the exact posterior to effictiently estimating its mean and covariance.
- EKF: statistics cannot be computed in closed form $->$ Linearization via Taylor expansion
- once the function $g$ describing the transformation between $x$ and $y$ is linearized, the mechanics of EKF's belief propagation are equivalent to those of the Kalman filter.
- likewise with the measurement function $h$ , when multiplying Gaussians. Again, the EKF apporximates $h$ by a linear function tangent $h$, thereby retaining the Gaussian nature of the posterior belief.
- Linearization: EKF uses 1st order Tayler expansion.
- Taylor expansion: linear approximation to a funtion $g$ from $g$'s value and slope, which is given by the partial derivative:

$$
g'(u_t, x_{t-1}):= \frac{\delta g(u_t,x_{t-1})}{\delta x_{t-1}}

$$

- $g$ and its slope depend on the argument of $g$ . For Gaussians the most likely state is the mean of the posterior $µ_{t-1}$, hence $g$ is approxdimated by $µ_{t-1}$ and its $u_t$
- -> Jacobian
- analog with the measurement function $h$

Differences between KF and EKF:

- linear predictions in KFs are replaced by their nonlinear generalizations in EKFs.
- EKF: uses Jacobians $G_t$ and $H_t$ instead of the corresponding linear system matrices $A_t$, $B_t$, $C_t$ in KFs
- The Jacobian $G_t$ corresponds to the system Matrices $A_t$, $B_t$ , and the Jacobian $H_t$ correponds to the measurement update matrix $C_t$.

Features of the Extended Kalman Filter:

- simple and computational efficient: as for the KF each update requires time $O(k^{2.4} + n^2)$, where $k$: dimension of measurement vector $z_t$, $n$: dimension of state vector $x_t$
- Compared to the particle filter, which requires time exponential in $n$
- EKF conputational effiecient, since belief is represented by gaussian distributions. Gaussian is unimodal: single guess annotated with an unvcertainty ellipse.
- EKFs have been applied to great success to a number of state estimation problems that violate the underlying assumptions.
- EKF: higher non linearities result in larger approximation errors.
- EKFs are incabable of representing multimodal beliefs.
- If nonlinear functions are approximately linear at the mean of the estate, then the EKF approximation of the posterior belief may be sufficiently good.
- The less certain the robot, ther wider its Gaussian belief, the more it is affected by nonlinearities in the state transition and measurement functions -> in practice: important to keep uncertainty of the state small.

Monte Carlo Approximation

### 1.3.3. ES-EKF
<a id="markdown-es-ekf" name="es-ekf"></a>
Linearization Error state EKF, whereas the normal EKF uses the full state. 
- also requires computing Jacobian to linearize nonlinear process model. 


### 1.3.4. Unscented Kalman Filter
<a id="markdown-unscented-kalman-filter" name="unscented-kalman-filter"></a>


- performs a stochastic linearization through the use of a weighted statistical linear regression process.
- instead of approximating the transfer function $g$ by a Taylor series expansion, the UKF deterministically extracts so called *sigma points* from the Gaussian and passes these through $g$, where $g$ is the nonlinear model function. 
- Instead of approximating the system equations by linearizing, the UKF algorithm calculates sigma points to uses the Unscented Transform to approximate the probability distribution function directly.
- in general sigma points are located at the mean of the gaussian and symetrically along the main axes of the covariance (two per dimension).
- For $n$-dimensional Gaussian with mean $µ$ and covariance $\sum$, the resulting $2n+1$ sigma points are chosen as follows:

Linear Regression:
- linear regression is a statistical process, that tries to describe an observable dependent variable (regressand) by one or more **independent** variables (regreesor). The regression is called "linear" since a linear model is assumed. 
Example: 
$$
Y_i = \beta_0 + \beta_1x_i + \epsilon_i, i = 1, ..., n 
$$

**SIGMA POINTS**
$$
\begin{aligned}
\Chi^{[0]} &= \mu \\

\Chi^{[i]} &= \mu + \bigg(\sqrt{(n+\lambda) \sum}\bigg)_i \space for \space i = 1,..., n\\
\Chi^{[i]} &= \mu - \bigg(\sqrt{( n+\lambda) \sum} \bigg)_i \space for \space i = n+1,..., 2n\\
\end{aligned}
$$ (3)

![UKF linerization](images/UKF_linearization.jpg)
  *UKF linearization*

$\lambda = \alpha^2(n+\kappa)-n$, with $\alpha$ and $\kappa$ being scaling parameter, determining how far the sigma points are spread from the mean of the gaussian to be passed through the function $g$, [1]. Each sigma point $\Chi^{[i]}$ has two weights associated with it: $w^{[i]}_m$ is used when computing the mean, $w^{[i]}$ is used when recovering the covariance of the Gaussian.



$$
\begin{aligned}
w^{[0]}_m  &= \frac{\lambda}{n+\lambda} \\
w^{[0]}_c  &= \frac{\lambda}{n+\lambda} + (1-\alpha^2 + \beta)
\end{aligned}
$$(4)

The linearization of the UKF first extract $2n+1$ weighted sigma points from the n-dimensional Gaussian ($n=1$ in the example in the figure above). These sigma points are passed through the nonlinear function $g$. The linearized Gaussian is then extracted from the mapped sigma points (small cirles in the upper right plot). As for the EKF, the linerization incurs an approximation error, indicated by the mismatch between the linearized Gaussian (dashed) and the Gaussian computed from the highly accurate Monte-Carlo estimate (solid). $\lambda$ is a parameter to set how far the sigma points are away from the mean.
$$
\begin{aligned}
w^{[i]}_m = w^{[i]}_c = \frac{1}{2(n+\lambda)} \space \space \space \textrm{for \space} i=1, ...,2n
\end{aligned}
$$(5)

The parameter $\beta$ can be chosen to encode additional (higher order) knowledge about the distribution underlying Gaussian representation. For an exact Gaussian: $\beta = 2$ is optimal. 
Parameters for computing the sigma points can be tweaked for the type of distribution: for gaussian it is $2$, since it has tow moments, and the rest of parameters are zero. 
The number of Sigma points to be chosen depends on the dimensionality $n$ of the probability distribution: 
$2n+1$.
The sigma points are then passed through the function $g$

$$
\gamma^{[i]} = g(\Chi^{[i]})
$$(6)

The parameters ($\mu'$, $\sum'$, moments) of the resulting Gaussian are extracted from the mapped sigma points $\gamma^{[i]}$ according to

$$
\mu' = \sum^{2n}_{i=0}w^{[i]}_m \gamma^{[i]}
$$(7)

The tranformed mean $\mu'$ is the weighted sum of transformed sigma points $\gamma^{[i]}$, where $n$ is the dimension of the Gaussian distribution, i.e. dimension of the state $x$.

**Transformed Covariance Matrix**:
$$
  \Sigma^{'} = \sum^{2n}_{i=0} w^{[i]}_c (\gamma^{[i]}- \mu')(\gamma^{[i]}- \mu')^T
$$(8)

**Unscented Transform**
How to select the parameters? 
- free parameters as there is no unique solution
- Scaled Unscented Transform suggests

$$
\kappa \geq 0 \\
\alpha \in (0,1]) \\
\lambda = \alpha^2(n+\kappa) - n \\ 
\beta = 2 
$$

- $\kappa$, $\alpha$ $\lambda$, influence how far the sigma points are away from the mean
- the larger $\lambda$ or $\kappa$ the further away are the sigma points from the mean
- $\beta$ parameter for the probability distribution, whereas $\beta = 2$ optimal choice for the gaussian.
- Scaled Unscented Transform is a generalization of the unscented transform.

**Comparison of UKF with EKF:**
- The asymptotic complexity of the UKD algorithm is the same as for the EKF.
- UKF is more accurate than the first order Taylor series expansion applied by the EKF. The UKF is accurate in the first tow order terms of the Taylor expansion, while the EKF captures only the first order term. Though, both the EKF and the UKF can be modified to capture higher order terms.
- pro: UKF does not require the computation of Jacobian: UKF is a *derivative-free filter*
- in many applications the difference between EKF and UKF is negligible

Check chapter 7 in [[1]](#1) for an UKF algorithm performing more accurate estiamtion of prediction and measurement noise terms.

![Kalman Filter types](images/KF_types_comparison.jpg)
*Kalman Filter types algorithm comparison*


Contraints for sampling the gaussian, that is to be passed through the non-linear system function $g$: 

$$\begin{aligned}
& \sum_i w^{[i]} = 1 \\
& \mu = \sum_i w^{[i]}\Chi^{[i]} \\
& \Sigma=\sum_i w^{[i]}(\Chi^{[i]}-\mu)(\Chi^{[i]}-\mu)^T \\
\end{aligned}
$$

The sum of all weight, that are used for the sampling the gaussian, should be equal to $1$. 
The weighted sum of sigma points $\Chi^{[i]}$ should equal the mean $\mu$ of the original gaussian distribution, likewise with the covoriance $\Sigma$ matrix.
There is no unique solution for $\Chi^{[i]}$, $w^{[i]}$
If the contstraints would not hold, and the unscented transform would be applied to identity, it would diverge.


**THE ALGORIHM BELOW IS ONLY APPLICABLE TO ADDITIVE PROCESS AND MEASUREMENT NOISE!**

**Unscented Kalman Filter Algorithm** 1: $\mu_{t-1}, \Sigma_{t-1} u_t, z_t$
$$
\begin{aligned}
  &2: \quad \quad \Chi_{t-1} = ( \mu_{t-1} \quad \mu_{t-1} + \gamma \sqrt{\Sigma_{t-1}} \quad \mu_{t-1} - \gamma \sqrt{\Sigma_{t-1}} ) \\
  &3: \quad \quad Y = \bar{\Chi}^*_t = g(u_t, \Chi_{t-1}) \\
  &4: \quad \quad \bar{\mu}_t = \sum^{2n}_{i=0} w^{[i]}_m \bar{X_t}^{*[i]} \\
  
  &5: \quad \quad \bar{\Sigma}_t = \sum^{2n}_{i=0} w_c^{[i]}(\bar{\Chi}^{*[i]} - \bar{\mu}_t) (\bar{\Chi}_t^{*[i]} - \bar{\mu}_t)^T + R_t \\
  &6: \quad \quad \bar\Chi{}_t = (\bar{\mu_t} \quad \bar{\mu_t} + \sqrt{\bar{\Sigma}} \quad \gamma - \sqrt{\bar{\Sigma}}) \\
  &7: \quad \quad \bar{\Zeta}_t = h(\bar{X}_t) \\
  &8: \quad \quad \hat{z}_t = \sum^{2n}_{i=0}w^{[i]}_m\bar{\Zeta}^{[i]}_t \\
  &9: \quad \quad S_t = \sum^{2n}_{i=0}w_c^{[i]}(\hat{\Zeta}^{[i]}_t-\hat{z}_t)(\hat{\Zeta}^{[i]}_t-\hat{z}_t)^T +Q_t \\
  &10: \quad \quad \bar{\Sigma}^{x,z}_t = \sum^{2n}_{i=0}w^{[i]}_c(\bar{\Chi}^{[i]}_t - \bar{\mu}_t)(\bar{\Zeta}^{[i]}-\hat{z}_t)^T \\
  &11: \quad \quad K_t = \bar{\Sigma}^{x,z}S^{-1}_t \\
  &12: \quad \quad \mu_t = \bar{\mu}_t + K_t(z_t-\bar{z}_t) \\
  &13: \quad \quad \Sigma_t = \bar{\Sigma_t}-K_t S_t K^T_t \\
  &14: \quad \quad return \space \mu_t, \Sigma_t
  \end{aligned}
$$(9)


to 5: $R_t$ is added to model additional prediction noise uncertainty. The unscented transform for the state (prediction step) has been carried out.

to 6: predicted belief $\bar{\Chi}_t$

to 6-11: Correction

to 10: determines the cross variance between the state and observation.

to 11: Kalman gain = crossvariance of state and measurement divided by uncertainty of measurement. The higher the latter ($S_t$) the lower the Kalman gain $K_t$. Contrary, the higher the cross variance between measurement and state, the higher the Kalman gain.

to 12-13: Estimation update (equivalent form as for EKF)


**UKF Algorithm Prediction step:**
1. Compute Sigma Points
2. Propagate Sigma points through non linear system model $g$
3. Compute predicted mean and covariance

**UKF Algorithm Correction step:**
To correct the state estimate using measurements at time $t$, the nonlinear measurement model $h$ and the sigma points computed in the prediction step are used to predict the measurements $\hat{z_t}$:

1. Predict measurements from propagated sigma points (substep 6) -> $\bar{Z_t}$
2. Estimate the mean and the covariance of predicted measurements $\hat{z}_t$ and $S_t$
3. Compute the cross-covariance $\bar{\Sigma}^{x,z}_t$ and Kalman Gain $K_t$ (substep 11)
4. Compute the corrected mean $\mu_t$ and covariance $\Sigma_t$ (substep 12-13)
   
|Kalman Gain  |Observation Uncertainty  |Crossvariance  |
|---------          |---------          |---------      |
|$K_t \uparrow$     | $S_t \downarrow$  |$\bar{\Sigma}^{x,z} \uparrow$ |
|$K_t \downarrow$   | $S_t \uparrow$  |$\bar{\Sigma}^{x,z} \downarrow$ |
*Dependence of Kalman Gain to Observation Uncertainty $S_t$ and Crossvariance between state and observation $\bar{\Sigma}^{x,z}_t$*

**Definitions**
|Quantity       |Definition   |
|---------------|-------------|
|$Q_{t}$       |covariance matrix of additive measurement noise at time $t$ |
|$\Chi_{t}$     |Sigma points of previous belief at time $t$ |
|$Y = \bar{\Chi}^*_{t}$| noise free state prediction at time $t$|
|$\bar{\Chi}_{t}$| new set of sigma points at time $t$, now capture the overall uncertainty after the prediction step|
|$\bar{\mu_{t}}$| predicted mean at time $t$|
|$\bar{\mu}_{t}$|             at time $t$|
|$x$            |   state at time $t$ |
|$z$            |   observation i.e measurement at time $t$|
|$\bar{\Sigma}_{t}$   | predicted covariance at time $t$|
|$\bar{\Sigma}^{x,z}_{t}$ |cross covariance between state $x$ and observation $z$ (measurement)  at time $t$, corresponds to $\bar{\Sigma}_tH^T_t$ for the EKF |
|$g$            | nonlinear model (system) function state prediction       |
|$h$            |measurement prediction function (to calculate predicted measurement for each sigma point)          |
|$R_t$          |additional prediction noise (additive), i.e. process noise|
|$S_t$          |Uncertainty of predicted oberservation $z_t$ at time $t$, represents the same uncertainty as $H_t\bar{\Sigma}_t H^T_t$ for the EKF              |
|$K_t$          |Kalman Gain  |
|$\hat{\Zeta}^{[i]}_t$| observation sigma points at time $t$ |
|$\bar{\Zeta}_t$| predicted observation (for each sigma point) at time $t$      |
$\hat{z}_t$| predicted (mean?) oberservation at time $t$ |
|$u_t$          | input to filter (control)       |
|*           |transformed (passed through function)|
|$\bar{a}$          | estimate of $a$             |
|$\hat{a}$          | estimate of $a$             |
*Defintions*

**Unscented Transform / UKF Summary**
- Unscented transforms as an alternative to linearization
- UT is a better approximation than Taylor expansion
- UT uses sigma point propagation
- free parameters in UT
- UKF uses the UT in the prediction and correction step



## 1.4. Kalman Filter bias estimation


### 1.4.1. Information Filter
<a id="markdown-information-filter" name="information-filter"></a>


- Is a dual of KF and uses cananonical parameterization

Multimodal distributions:

### 1.4.2. Histogram Filter
<a id="markdown-histogram-filter" name="histogram-filter"></a>

### 1.4.3. Particle filter
<a id="markdown-particle-filter" name="particle-filter"></a>

# 2. Sensor Fusion using Kalman Filter
<a id="markdown-sensor-fusion-using-kalman-filter" name="sensor-fusion-using-kalman-filter"></a>

Example: GPS and accelerometer,
- accelerometer used as input $u$ and GPS as measurements $z$ yields to fused sensor information. 

# 3. Appendix
<a id="markdown-appendix" name="appendix"></a>


**Gesetz der großen Zahlen:**

**Monte-Carlo Simulation:**

MCS is a method in stochastics using a large amount of the same random experiments to solve problems, which are very difficult to solve analytically or are not at all solvable analytically. These problems are solved numerically with the aid of the probability theory, when using MCS. The random process are carried out using Monte-Carlo Algorithms.




Example: movement estimation:

IMU data as input $u$ not as measurement data. [[8]](#8),[[9]](#9)

There are two ways to decompose the covariance matrix
# 4. Cholesky Decomposition (for Covariance matrix $\Sigma$)
<a id="markdown-cholesky-decomposition-for-covariance-matrix-%24%5Csigma%24" name="cholesky-decomposition-for-covariance-matrix-%24%5Csigma%24"></a>
- decomposes a symmetric positiv definit matrix into a product of lower triangular matrix and its transpose.
- pro: converging, more numerically stable solution, than matrix sqaure root
- often used in UKF implementations
- $L$ and $\Sigma$ have the same Eigenvectors


$$
\begin{aligned}
  \Sigma &= LL^T \\
         &= V\begin{pmatrix}
  d_{11} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& d_{nn} \\
  \end{pmatrix} V^-1 \\
  &= V\begin{pmatrix}
  \sqrt{d_{11}} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& \sqrt{d_{nn}}
 \end{pmatrix}
 \begin{pmatrix}
  \sqrt{d_{11}} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& \sqrt{d_{nn}}
 \end{pmatrix}V^-1
  \end{aligned}
$$



**Matrix Square Root** (for Covariance matrix $\Sigma$)
- defined as $S$ with $\Sigma = SS$
- Computed via diagonalization
- Eigenvalues are represented by the diagonal elements of the matrices in the last line of equation above. 
Two orthogonal vectors along the main diagonal forming an ellipse (2D Gaussian) are eigenvectors. If both eigenvalues are the same, the ellipse is a circle.  

$$
\begin{aligned}
  \Sigma &= VDV^-1 \\
         &= V\begin{pmatrix}
  d_{11} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& d_{nn} \\
  \end{pmatrix} V^-1 \\
  &= V\begin{pmatrix}
  \sqrt{d_{11}} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& \sqrt{d_{nn}}
 \end{pmatrix}
 \begin{pmatrix}
  \sqrt{d_{11}} &\cdots& 0 \\
  0 &\ddots & 0 \\
  0 &\cdots& \sqrt{d_{nn}}
 \end{pmatrix}V^-1
  \end{aligned}
$$


Todo: how does this work with the formulas given by the UKF algorithm?

#### 4.0.0.1. Positive definit matrix
<a id="markdown-positive-definit-matrix" name="positive-definit-matrix"></a>
Every quadratic matrix describes a bilinear form on $V= \R^n$. A quadratic matrix is positiv definit:

$$
\begin{aligned}
  \textrm{positive definit if,} \quad x^TAx>0 \\
  \textrm{positive semidefinit}, \quad x^TAx\geq0 \\
  \textrm{negative  definit}, \quad x^TAx<0 \\
  \textrm{negative semidefinit}, \quad x^TAx\leq0\\
\end{aligned}
$$

for all $n$-row column vectors $x\in V$ with $x\neq 0$. This is euqivalent to all eigenvalues of the quadratic matrix $A$ are positive, positive or equal to zero, negative or equal to zero, repsectively.

A matrix, which is not positive nor negative semidefinit, is called indefinit: Then $x^TAx$ can take positive an well as negative values.

**Bilinear Form**
bilinear form describes a linear function in linear algebra, which maps a scalar to two vectors and which is linear w.r.t to its two arguments, example: scalar product.

# 5. RESOURCES:
<a id="markdown-resources%3A" name="resources%3A"></a>

[1] Probabilistic Robots - Thrun, 2005
<a id="1" name="[1]"></a>

[2] Landau Symbol, <https://de.wikipedia.org/wiki/Landau-Symbole>
<a id="2" name="[2]"></a>

[3] <https://de.wikipedia.org/wiki/Monte-Carlo-Simulation>
<a id="3" name="[3]"></a>

[4] <https://de.wikipedia.org/wiki/Monte-Carlo-Algorithmus>
<a id="4" name="[4]"></a>

[5] <https://de.wikipedia.org/wiki/Gesetz_der_großen_Zahlen>
<a id="5" name="[5]"></a>

[6] Lineare Regression, https://de.wikipedia.org/wiki/Lineare_Regression
<a id="6" name="[6]"></a>

[7] https://www.coursera.org/lecture/state-estimation-localization-self-driving-cars/lesson-6-an-alternative-to-the-ekf-the-unscented-kalman-filter-voRRb
<a id="7" name="[7]"></a>

[8] https://youtu.be/DWDzmweTKsQ
<a id="8" name="[8]"></a>

[9] Lange, S. (2008). Mono-Kamera-SLAM. VDM, Saarbrücken, 1. edition.
<a id="9" name="[9]"></a>

[10] Langelaan, J. W. (2006). State estimation for autonomous flight in cluttered environments. PhD thesis, Stanford University.
<a id="10" name="[10]"></a>
